//
//  T1DrawingPoint.h
//  T1PogoManagerTest
//
//  Created by Peter Skinner on 2/18/13.
//  Copyright (c) 2013 Ten One Design LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface T1DrawingPoint : NSObject

@property (nonatomic, assign) CGPoint location;
@property (nonatomic, assign) CGPoint velocity;
@property (nonatomic, assign) CGPoint acceleration;
@property (nonatomic, assign) NSTimeInterval timestamp;
@property (nonatomic, assign) float pressure;
@property (nonatomic, assign) float diameter;
@property (nonatomic, assign) NSUInteger id;

@end
