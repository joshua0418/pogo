//
//  T1AppDelegate.m
//  T1PogoManagerDemo
//
//  Created by Peter Skinner on 11/2/11.
//  Copyright (c) 2011 Ten One Design. All rights reserved.
//

#import "T1AppDelegate.h"
#import "T1ViewController.h"

@implementation T1AppDelegate

@synthesize window = _window;
@synthesize viewController = _viewController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
	self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
	// Override point for customization after application launch.
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
		self.viewController = [[T1ViewController alloc] initWithNibName:@"T1ViewController_iPhone" bundle:nil];
	}
	else {
		self.viewController = [[T1ViewController alloc] initWithNibName:@"T1ViewController_iPad" bundle:nil];
	}
	self.window.rootViewController = self.viewController;
	[self.window makeKeyAndVisible];
	return YES;
}

@end
