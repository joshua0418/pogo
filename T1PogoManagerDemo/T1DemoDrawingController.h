#import <UIKit/UIKit.h>



@class PaintingBrush;



@interface T1DemoDrawingController : UIViewController {
	BOOL drawing;
	PaintingBrush *circleBrush;
}

@property (retain) UIView *drawingView;
@property (assign) id myController;
@property (nonatomic) BOOL inkEnabled;
@property (strong) NSMutableArray *drawingArray;
@property (strong) NSMutableArray *drawingArrayForRedo;
@property (strong) NSMutableArray *pointArray;

- (id)initWithView:(UIView *)theView;
- (void)setDrawingViewFrame:(CGRect)frame;
- (void)drawTouchBeganWithPoint:(CGPoint)touchPoint pressure:(float)pressure timestamp:(NSTimeInterval)timestamp;
- (void)drawTouchMovedWithPoint:(CGPoint)touchPoint pressure:(float)pressure timestamp:(NSTimeInterval)timestamp;
- (void)drawTouchEndedWithTimestamp:(NSTimeInterval)timestamp;
- (void)drawTouchChangedPressure:(float)pressure timestamp:(NSTimeInterval)timestamp;

- (void)setColor:(UIColor *)color;

// New methods for Ink Rendering
- (void)undoDrawingStroke;
- (void)resetCanvas;

@end
