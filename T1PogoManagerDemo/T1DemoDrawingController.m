#import "T1DemoDrawingController.h"
#import "PaintingBrush.h"
#import "T1DrawingPoint.h"
#import "T1DrawingView.h"
#import <QuartzCore/QuartzCore.h>



static const CGFloat kInkViewWidth  = 656.0f;
static const CGFloat kInkViewHeight = 680.0f;



@interface T1DemoDrawingController ()

@property (nonatomic, strong) T1DrawingView *canvasView;

@end



@implementation T1DemoDrawingController



@synthesize drawingView;
@synthesize myController;
@synthesize inkEnabled;



- (id)initWithView:(UIView *)theView {
	if (self = [super init]) {
		self.inkEnabled  = YES;
		self.drawingView = theView;
		circleBrush      = [[PaintingBrush alloc] initWithFrame:drawingView.bounds];
		[circleBrush setCircleStrokeColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:.8]];
		[circleBrush setCircleStrokeSize:3];
        
		// stuff for drawing a line
		self.drawingArray = [NSMutableArray array];
		self.drawingArrayForRedo = [NSMutableArray array];
		self.pointArray = [NSMutableArray array];
        
		self.canvasView = [[T1DrawingView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, kInkViewWidth, kInkViewHeight)];
		[self.canvasView setTag:90];                // so we can identify when sorting
		[self.canvasView setDrawSmoothCurve:YES];
		[self.canvasView setUserInteractionEnabled:NO];
		[self.canvasView setStrokeColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1]];
		[drawingView addSubview:self.canvasView];
	}
	return self;
}

- (void)setDrawingViewFrame:(CGRect)frame {
	[self.drawingView setFrame:frame];
	[circleBrush setFrame:drawingView.bounds];
}

- (void)setColor:(UIColor *)color {
	[circleBrush setCircleStrokeColor:color];
	self.canvasView.strokeColor = color;
}

- (void)drawTouchBeganWithPoint:(CGPoint)touchPoint pressure:(float)pressure timestamp:(NSTimeInterval)timestamp {

	if (drawing) {
		[self drawTouchEndedWithTimestamp:timestamp];
	}
    
	drawing = YES;
    
	if (self.inkEnabled == YES) {
		// draw line
		self.pointArray = [[NSMutableArray alloc] init];
		[self.drawingArray addObject:self.pointArray];
        
		T1DrawingPoint *pointObject = [[T1DrawingPoint alloc] init];
		pointObject.location = touchPoint;
		pointObject.timestamp = timestamp;
		pointObject.velocity = CGPointMake(0, 0);
		pointObject.pressure = pressure;
		[self drawNextPoint:pointObject];
	}
    
	// draw circle
//	circleBrush.size = pressure * 60;
//	[circleBrush setCircleStrokeSize:3 * (1 + pressure / 2)];
//	circleBrush.location = touchPoint;
//	[self.drawingView addSubview:circleBrush];
//	[circleBrush setNeedsDisplay];
}

- (void)drawTouchMovedWithPoint:(CGPoint)touchPoint pressure:(float)pressure timestamp:(NSTimeInterval)timestamp {

	if (!drawing) {
		[self drawTouchBeganWithPoint:touchPoint pressure:pressure timestamp:timestamp];
	}
	else {
		if (self.inkEnabled == YES) {
			// draw line
			T1DrawingPoint *pointObject = [[T1DrawingPoint alloc] init];
			pointObject.location = touchPoint;
			pointObject.timestamp = timestamp;
			pointObject.velocity = CGPointMake(0, 0);
			pointObject.pressure = pressure;
			[self drawNextPoint:pointObject];
		}
        
		// draw circle
//		circleBrush.size = pressure * 60;
//		[circleBrush setCircleStrokeSize:3 * (1 + pressure / 2)];
//		circleBrush.location = touchPoint;
//		[circleBrush setNeedsDisplay];
	}
}

- (void)drawTouchEndedWithTimestamp:(NSTimeInterval)timestamp {

	if (drawing) {
		if (self.inkEnabled == YES) {
			// for line
			[self.drawingArrayForRedo removeAllObjects];
			[self updateCanvasWithDrawingArray:self.drawingArray finalizeStroke:YES fullRedraw:NO];
		}
        
		// for circle
//		[circleBrush removeFromSuperview];
	}
    
	drawing = NO;
}

- (void)drawTouchChangedPressure:(float)pressure timestamp:(NSTimeInterval)timestamp {
	// these come through because the pressure signal may change in between the pen's touch events
	// the right thing to do in this situation is to redraw the last drawing point with greater pressure
    
	if (drawing) {
		T1DrawingPoint *point = [self.pointArray lastObject];
		point.pressure = pressure;
		point.diameter = [self calculateThicknessWithVelocity:point.velocity andPressure:point.pressure];
		[self updateCanvasWithDrawingArray:self.drawingArray finalizeStroke:NO fullRedraw:NO];
        
        
		// draw circle
		circleBrush.size = pressure * 60;
		[circleBrush setCircleStrokeSize:3 * (1 + pressure / 2)];
		[circleBrush setNeedsDisplay];
	}
}

#pragma mark - Ink Rendering Extra Code



- (void)drawNextPoint:(T1DrawingPoint *)point {
	point.id = 10;
    
	point.diameter = [self calculateThicknessWithVelocity:point.velocity andPressure:point.pressure];
    
	[self.pointArray addObject:point];
    
	[self updateCanvasWithDrawingArray:self.drawingArray finalizeStroke:NO fullRedraw:NO];
}

- (CGFloat)calculateThicknessWithVelocity:(CGPoint)touchVelocity andPressure:(float)pressure {
	return 0.5 + pressure * 29.5;
}

- (void)updateCanvasWithDrawingArray:(NSMutableArray *)drawingArray finalizeStroke:(BOOL)finalizeStroke fullRedraw:(BOOL)fullRedraw {
	[self.canvasView setMyStrokes:drawingArray];
	[self.canvasView drawToCacheWithFinalSegment:finalizeStroke fullRedraw:fullRedraw];
}

- (void)cancelDrawingStroke {
	if (drawing) {
		[self.drawingArray removeLastObject];
        
		drawing = NO;
		[self updateCanvasWithDrawingArray:self.drawingArray finalizeStroke:YES fullRedraw:YES];
	}
}

- (void)undoDrawingStroke {

	if ([self.drawingArray count] > 0) {
		if (drawing == NO) {    // disable redo for this stroke if it was incomplete
			[self.drawingArrayForRedo addObject:[self.drawingArray objectAtIndex:[self.drawingArray count] - 1]];
		}
		[self.drawingArray removeLastObject];
        
		[self updateCanvasWithDrawingArray:self.drawingArray finalizeStroke:YES fullRedraw:YES];
	}
	drawing = NO;
}

- (void)resetCanvas {
	[self cancelDrawingStroke];
	[self.drawingArray removeAllObjects];
	[self.drawingArrayForRedo removeAllObjects];
	[self updateCanvasWithDrawingArray:self.drawingArray finalizeStroke:YES fullRedraw:YES];
}

@end
